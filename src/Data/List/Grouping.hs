{-# LANGUAGE TupleSections #-}
module Data.List.Grouping where

import Data.List (foldl')
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Arrow (second)
import Data.Array

-- | Group a list of (key,value) pairs by their key, placing each value into a set
groupList :: (Ord k, Ord v) => [(k,v)] -> M.Map k (S.Set v)
groupList = foldl' (\acc (k,v) -> M.insertWith S.union k v acc) M.empty . map (second S.singleton)

-- | Group a list of (key,value) pairs by their key
groupList' :: (Ord k, Ord v) => [(k,v)] -> [(k,[v])]
groupList' = M.toAscList . fmap S.toAscList . groupList

-- | Make an array of length i, with all of the bits in list N set to True and all other bits set to False
toggleBits :: Int -> [Int] -> Array Int Bool
toggleBits i xs = let baseArray = listArray (0,i) $ replicate i False in baseArray // map (,True) xs

-- | Make a list A of length i, with all of the bits in the supplied list set to True and all other bits set to False
toggleBits' :: Int -> [Int] -> [(Int,Bool)]
toggleBits' i xs = assocs $ toggleBits i xs

-- | As with toggleBits' but gets only the bit list without the associated indexes
toggleBits'' :: Int -> [Int] -> [Bool]
toggleBits'' i xs = elems $ toggleBits i xs

-- | Group a list into a map with their ordinal indexes as keys
groupToMap :: [a] -> M.Map Int a
groupToMap = M.fromList . zip [0..]