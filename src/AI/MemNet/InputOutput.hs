module AI.MemNet.InputOutput where

import Control.Concurrent.MVar
import qualified Data.Map as M
import Data.List (splitAt)
import Control.Applicative
import Control.Monad
import Data.Maybe
import Data.Array
import Data.IORef

import AI.MemNet.MemNode

-- | Type alias for the mutable variable used on the IO portion of MemNet.
--   Currently an IORef, and kept in one place so that its easy to change
--   to an MVar later if it becomes unsuitable.
type InputOutput a = MVar a

-- | Function alias for newIORef
newInputOutput :: a -> IO (InputOutput a)
newInputOutput = newMVar

-- | Function alias for readIORef
readInputOutput :: InputOutput a -> IO a
readInputOutput = readMVar

-- | Function alias for writeInputOutput
writeInputOutput :: InputOutput a -> a -> IO ()
writeInputOutput mv x = modifyMVar_ mv (const $ return x)

-- | Type alias to distinguish IO inputs from outputs
type Input a = InputOutput a

-- | Type alias to distinguis IO outputs from inputs
type Output a = InputOutput a

-- | Type alias for the shutdown command
type Shutdown = Input Bool

-- | Create a new shutdown command
newshutdown :: IO Shutdown
newshutdown = newMVar False

-- | Shut down all of the things
shutdown :: Shutdown -> IO ()
shutdown shut = writeInputOutput shut True

-- | Type alias for mutable variable that keeps track of the number of cycles
type CycleCount = Output Int

-- | Type alias for input groupings
type InputGroup = [Input [(Int,Bool)]]

-- | Type alias for output groupings
type OutputGroup = [Output [(Int,Bool)]]

-- | Function alias for creating a new mutable IO
newInputOutputGroup :: [Bool] -> IO (InputOutput [(Int,Bool)])
newInputOutputGroup = newInputOutput . zip [0..]

-- | Modify a mutable IO state with a function
modifyInputOutput :: InputOutput a -> (a -> a) -> IO ()
modifyInputOutput bs g = modifyMVar_ bs (return . g)
-- modifyInputOutput :: InputOutput a -> (a -> a) -> IO ()
-- modifyInputOutput bs g = let g' bs' = (bs',g bs') in void $ atomicModifyIORef' bs g'

-- | Given a list of bits, modify the first bit with a value
modifyFirstBit :: Bool -> [(Int,Bool)] -> [(Int,Bool)]
modifyFirstBit b xxs@((i,_):xs) = (i,b):xs
-- modifyFirstBit :: Bool -> [(Int,Bool)] -> ([(Int,Bool)],[(Int,Bool)])
-- modifyFirstBit b xxs@((i,_):xs) = (xxs,(i,b):xs)

-- | Given a list of bits, modify the second bit with a value
modifySecondBit :: Bool -> [(Int,Bool)] -> [(Int,Bool)]
modifySecondBit b xxs@(x:(i,_):[]) = [x,(i,b)]

-- | Given a list of bits, modify the Nth bit with a value
modifyNthBit :: Int -> Bool -> [(Int,Bool)] -> [(Int,Bool)]
modifyNthBit 0 b bs = modifyFirstBit b bs
modifyNthBit 1 b bs = modifySecondBit b bs
modifyNthBit n b bs = let (xs,(i,_):ys) = splitAt n bs in xs ++ (i,b):ys

-- | Modify a particular bit of a particular mutable value
atomicModifyNthBit :: InputOutput [(Int,Bool)] -> Int -> Bool -> IO ()
atomicModifyNthBit bs n b = modifyInputOutput bs (modifyNthBit n b)

-- | In order to dictate the speed at which the network operates
--   a simple lock is needed.
type CycleGate = Input [MVar ()]
-- TODO: This was a terrible idea... there should be a better way

-- | The Mediator will stop at the gate until the UI provides a go instruction
stopAtGate :: CycleGate -> IO ()
stopAtGate cg = do
    (x:xs) <- readInputOutput cg
    readMVar x
    y <- newEmptyMVar
    writeInputOutput cg (y:xs)

-- | The UI should open the gate at the regular intervals it wants the network to run
openGate :: CycleGate -> IO ()
openGate cg = do
    (x:_) <- readInputOutput cg
    putMVar x ()
    -- writeInputOutput cg (x:y:xs)

-- | The `startMemNet` function requires a UI that it can run. Any
--   UI that operates in a loop will suffice, be it GUI of CLI
class NetworkUI ui where
    runUI :: ui -> IO ()

-- | All of the required mutable variables to operate a MemNet
--   as well as the MemNet UI
data NetworkIO ui = NetworkIO
    { getUI :: ui
    , getShutdown :: Shutdown
    , getCounter :: CycleCount
    , getInputs :: InputGroup
    , getOutputs :: OutputGroup
    , getGate :: CycleGate
    }
