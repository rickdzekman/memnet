{-# LANGUAGE RecordWildCards, TupleSections, BangPatterns #-}
module AI.MemNet.Server where

import Control.Applicative
import qualified Data.Map as M
import Data.Array
import Control.Monad

import Control.Concurrent.MVar
import Control.Concurrent

import Control.Monad.Tuple
import Data.List.Grouping

import AI.MemNet.Builder
import AI.MemNet.MemNode
import AI.MemNet.InputOutput

{- | For quick lookups there is a map from each Node to its corresponding bit
     in an Input or Output. There is also a reversed maping, which contains
     sub-maps from each input or output parent, to their individual bits and
     which node's they connect to.
-}
data ReverseMap = ReverseMap
    { toNetwork :: Array Int (Array Int NID)
    , fromNetwork :: M.Map NID (Int,Int)
    }

-- | Create a reverse map
createReverseMap :: M.Map NID (Int,Int) -> ReverseMap
createReverseMap mns = ReverseMap arrayConvert mns
    where
        concatNids = map (map snd) . map snd . groupList' . map (\(n,(a,b)) -> (a,(b,n))) $ M.assocs mns
        arrayConvert = listArray (0,length concatNids - 1) $ map (\xs -> listArray (0,length xs -1) xs) concatNids


-- | The Mediator is where the real asynchronous plumbing happens.
data Mediator = Mediator
    { mediateShutdown :: Shutdown -- ^ Shutdown command
    , mediateCounter :: CycleCount -- ^ Keeps track of the number of cycles
    , mediateInputs :: M.Map Int (Input [(Int,Bool)]) -- ^ Mutable state for each input
    , mediateOutputs :: M.Map Int (Output [(Int,Bool)]) -- ^ Mutable state for each output
    , mediateNodes :: NodeGroup -- ^ Nodes prior to entering the ST monad
    , mediateNodeInput :: ReverseMap -- ^ Lookup and reverse lookup for nodes and Input bits
    , mediateNodeOutput :: ReverseMap -- ^ Lookup and reverse lookup for nodes and Output bits
    , mediateGates :: CycleGate -- ^ The Mediator locks on this gate at the start of every cycle
    , mediateInputSemaphore :: Semaphore [(Int,[Int])] -- The Mediator causes the ST based network to lock on this semaphore at the start of each cycle
    , mediateOutputSemaphore :: Semaphore [(Int,[Int])] -- The Mediator locks on this sempahor waiting from data from the ST network
    }

-- | Create a Mediator based on a build `Schema` and all of the mutable states
--   from the interface.
createMediator :: Schema -> NetworkIO ui -> IO Mediator
createMediator sch NetworkIO{..} =  Mediator 
                                        getShutdown
                                        getCounter
                                        inmap
                                        oumap
                                        nodeList
                                        reverseIn
                                        reverseOut
                                        getGate
                                    <$> newSemaphore []
                                    <*> newSemaphore []
    where
        interims = parseMultiple sch
        nodeList = M.elems $ interimNodes interims
        reverseIn = createReverseMap $ interimInputs interims
        reverseOut = createReverseMap $ interimOutputs interims
        inmap = groupToMap getInputs
        oumap = groupToMap getOutputs


-- | Pass this function the mediator from `createMediator` as well as `runMemNet` from `AI.MemNet.RunMemNet`
startServer :: Mediator -> (Mediator -> IO ()) -> IO ()
startServer m@Mediator{..} g = void (forkIO (g m)) >> loop
    where
        loop :: IO ()
        loop = readInputOutput mediateShutdown >>= \shut -> unless shut $ do
            stopAtGate mediateGates
            modifyInputOutput mediateCounter succ
            c <- readInputOutput mediateCounter
            ins <- collectAllInputs mediateInputs
            putSemaphore mediateInputSemaphore ins
            outs <- runSemaphore mediateOutputSemaphore
            notifyAllOutputs mediateOutputs outs
            loop

-- | Locking mechanism between the Mediator and the ST network
type Semaphore v = MVar ([MVar ()],MVar v)

-- | Initialise a semaphore
newSemaphore :: v -> IO (Semaphore v)
newSemaphore !v = do
    mv <- newMVar v
    x1 <- newEmptyMVar
    x2 <- newEmptyMVar
    let xs = [x1,x2]
    newMVar (xs,mv)

-- | Attempt to take a value from the semaphore, locking if it's not ready
runSemaphore :: Semaphore v -> IO v
runSemaphore !mvs = do
    (s:_,_) <- readMVar mvs
    readMVar s
    (_:ss,mv) <- takeMVar mvs
    s2 <- newEmptyMVar
    putMVar mvs (s2:ss,mv)
    readMVar mv

-- | Put something in the Semaphor, unlocking in the process
putSemaphore :: Semaphore v -> v -> IO ()
putSemaphore !mvs !v = do
    (s:ss,mv) <- readMVar mvs
    modifyMVar_ mv (const $ return v)
    putMVar s ()


-- | From the mutable states of all inputs, collect data to pass to
--   the network
collectAllInputs :: M.Map Int (Input [(Int,Bool)]) -> IO [(Int,[Int])]
collectAllInputs mixs = do
    let 
        inMap (ix,ibs) = do
            bs <- readInputOutput ibs
            let nbs = map fst . filter snd $ bs
            return (ix,nbs)
    mapM inMap $ M.assocs mixs

-- | With data from the network, and the mutable state of all outputs, send
--   an update to each output's mutable state.
notifyAllOutputs :: M.Map Int (Output [(Int,Bool)]) -> [(Int,[Int])] -> IO ()
notifyAllOutputs mixs nis = do
    -- allOuts and the mapM_ below are a bit of a hack. They are necessary to
    -- reset all outputs to [False] lists before the network updates them.
    -- TODO: Find a better way to do this
    allOuts <- mapM (\v -> mfst (readInputOutput v,v)) $ M.elems mixs
    mapM_ (\(v,mv) -> writeInputOutput mv $ map (,False) [0..length v - 1]) allOuts
    let outMap (ix,xs) = do
            let out = M.lookup ix mixs
            case out of 
                Nothing -> do 
                                print $ "Error: Could not find index " ++ show ix ++ " in output map"
                                return ()
                Just ior -> do
                                bs <- readInputOutput ior
                                let l = length bs
                                    xs' = toggleBits' l xs
                                writeInputOutput ior xs'
    mapM_ outMap nis