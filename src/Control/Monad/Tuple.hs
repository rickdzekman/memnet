{-# LANGUAGE TupleSections #-}
module Control.Monad.Tuple (mfst, msnd, mjoin, mjoin') where

import Control.Applicative

{-
    | Four useful function for dealing with Monads/Functors
      inside Tuples, all returning m (a,b).
-}

-- TODO: These functions should probably have better names

-- | Join two Monads into a tuple. For lists would create
-- a cartesian product. Equivalent to `liftM2 (,)` or the
-- Applicative `(<*>) . ((,) <$>)`
mjoin :: (Monad m) => m a -> m b -> m (a,b)
mjoin ma mb = ma >>= \a -> mb >>= \b -> return (a,b)

-- | Uncurried version of mjoin. Also a cartesian product 
-- for lists. Equivalent to `uncurry $ liftM2 (,)` or the
-- Applicative `uncurry ((<*>) . ((,) <$>))`
mjoin' :: (Monad m) => (m a,m b) -> m (a,b)
mjoin' (ma,mb) = mjoin ma mb

-- | Use when fst is a Functor to make the tuple a Functor.
-- Done point(less)-free uncurry $ flip ((<$>) . flip (,))
mfst :: (Functor m) => (m a,b) -> m (a,b)
mfst (ma,b) = (,b) <$> ma

-- | Use when snd is a Functor to make the tuple a Functor.
-- Done point(less)-free  `uncurry ((<$>) . (,))`
msnd :: (Functor m) => (a,m b) -> m (a,b)
msnd (a,mb) = (a,) <$> mb