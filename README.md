# MemNet v0.1.0.0

## About MemNet
MemNet is a proof-of-concept prototype for a stateful Artificial Neural Network (ANN) that
is capable of short term memory. This short term memory comes in the form of emulating
a 1-bit register. The design of the register is similar to a DFF based hardware register.

Unlike many ANN designs, the MemNet is not meant to be fed a single batch of data and output
an eventual calculation. It is instead meant to be always active, constantly receiving data
from its inputs and constantly streaming data from its outputs. In this way it is an ANN
design which hopes to eventually express human-like "Short Term Memory" but represented
in binary terms.

## Quick start
Note that:

* If you're on Windows the last line is MemNet.exe +RTS -N4
* The +RTS -NX argument is based on how many cores your CPU has. Though this demo is so simple the arguments can be dropped entirely without any impact

To get up and running download to a directory and install into a Cabal Sandbox using

    $ cabal sandbox init
    $ cabal install -j
    $ cd dist/build/MemNet
    $ MemNet +RTS -N4

This will open up a (terribly made) GUI. The GUI will display:

1. An incrementing count of what cycle the network is currently up to
2. The current value being output by the register in the network
3. The current input value being fed into the register

Press UP/DOWN on your keyboard modifies the "input value" being sent to the register.
As this is a 1-bit register the min/max is 0 and 1 respectively. Pressing ENTER will, 
for 1 cycle, submit a "write" instruction to the register.

If you press UP to change the input value to 1, it will send that to the MemNet but
it will not get written to the register. If you press ENTER a second bit of
information is sent to the register but as previously stated, only for one cycle. In
exactly 3 cycles after submitting the value the MemNet will begin to output the
written value.

Re-submitting the same value again has no effect, while submitting a new value will
override the output in 3 more cycles.

## How it works
### Background - hardware registers
The aim of the project is to show how a Neural Network can emulate a 
register on a conventional computer. Background reading for registers:

* [Data Flip Flop](http://en.wikipedia.org/wiki/Flip-flop_%28electronics%29#D_flip-flop)
* [Data Register - Memory](http://en.wikipedia.org/wiki/Memory_data_register)
* [Data Register - Processor](http://en.wikipedia.org/wiki/Processor_register)

### Short Term Memory via registers
The short-term memory is created by a feedback-loop which is constantly
outputting the last bit that was written to memory. To write to the short
term memory requires two components:
1 A bit toggle bit that tells the register that incoming data is to be written into the register
2 A bit that specifies the value that is to be written into the register

Human Short Term Memory functions not (quite entirely) unlike this. Namely
that we have an active "attention" and things remain in our attention while
we focus on them. This is achieved by a set of highly complex feedback loops
in the brain. Once something leaves the feedback loop it leaves our short 
term memory.

### Stateful Network
MemNet runs in a constant loop. It is always outputting the data that was
last written to the register. To achieve this the network runs a set of
"phases" through a cycle. The cycle is repeated until a shutdown command
is sent.

There are 3 distinct components:

1. The network itself, which lives in the `ST` Monad and is threaded through each cycle inside of an `IO ()` loop
1. A `Mediator` which sends inputs/receives outputs to the network, throttled by a pair of Semaphores
1. A `NetworkUI` which communicates to the Mediator and throttles its operations via simple lock called a `CycleGate`

The stateful properties are contained within the ST Monad, preventing any
data from the network from Leaking. All interactions with the outside
world are passed via IO through the Mediator.

## Making it run faster
For demonstration purposes the MemNet has been throttled to 2 cycles every second.
This makes it easy to visualise the "3 cycle delay" of updating the register.
Because each of the 3 stateful components operate asynchronously there are a set
of careful placed locks and semaphores that ensure smooth operation.

In a CPU the maximum speed of the clock cycle is determined by the shortest amount
of time it takes for the tick-tock of the CPU's clock to reach the farthest
component of the machine. There will be a theoretical limit to the speed at which
MemNet cycles can operate based on the size of the MemNet and the speed of the
computer that is running it. At this speed the lock/semaphore mechanism will
behave unpredictably.

With a simple 1-bit register this should be a fairly high threshold. Should you
wish to experiment with the speed, the simplest place to start is by modifying
`AI.MemNet.MemTest`. The function `testUI` creates an interface using
`Graphics.Gloss.Interface.IO.Game`.

To test faster speeds the function that's used is playIO and has the following
type signature:

    playIO  :: forall world
            .  Display
            -> Color
            -> Int
            -> world
            -> (world -> IO Picture)
            -> (Event -> world -> IO world)
            -> (Float -> world -> IO world)
            -> IO ()

Changing the `Int` parameter to a higher number will yield a larger number of cycles
per second. Gloss's Game UI is designed for reasonably simple game interfaces and
will have its own frame rate limit, likely to be much smaller than the cycle count
that can be handled by the 1-bit MemNet.

### Adding extra nodes
It should be possible to create an alternative to `MemTest` using any configuration.
This includes any number of nodes, inputs and outputs. Creating and connecting nodes
is fairly straight forward. The functionality is provided by `AI.MemNet.Builder` and
simply involves creating a list of nodes and edges.

Creating Inputs and Outputs however is a much more difficult process. Each input and
output must be represented by a list of bits paired with their index: `[(Int,Bool)]`.

The input and output groups are represented by the following type aliases:

    -- | Type alias for input groupings
    type InputGroup = [Input [(Int,Bool)]]

    -- | Type alias for output groupings
    type OutputGroup = [Output [(Int,Bool)]]

Either of which would desugar to:

    [MVar [(Int,Bool)]]

Using `AI.MemNet.Builder` you can connect nodes to their outputs and inputs by specifying
a tree-like path the node would have to follow through a `[MVar [(Int,Bool)]]` before
it would get to its corresponding bit. The example in MemTest is given as so:

    {-...-}
    -- Outputs
    , MakeOutput 2 0 0 -- From Node 2, to output 0, and bit 0 of that output
    -- Inputs
    , MakeInput 4 0 0 -- Input to Node 4, from input 0, and bit 0 of that input
    , MakeInput 5 0 1 -- Input to Node 5, from input 0, and bit 1 of that input
    {-...-}

By far the most complex part of adding Inputs and Outputs is the interface component,
which involves a careful choreography of asynchronous data exchanges too detailed to
describe in a short readme.


## Future of this project
MemNet is a brief detour from broader AI research. There are no plans to develop
this further at this stage. Should you have any interest you can feel free to
contact the author, preferably via Twitter: http://twitter.com/rickdzekman