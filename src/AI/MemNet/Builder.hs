{-# LANGUAGE RecordWildCards #-}
module AI.MemNet.Builder where

import AI.MemNet.MemNode
import Control.Monad.State
import qualified Data.Map as M
import Control.Applicative

-- | Simple type alias for a list of `NodeInstruction`s that can be used to build a network
type Schema = [NodeInstruction]

-- | Each instruction is parsed in turn to provide the final network. All nodes must be made
--   first before the edge, input or output componnets are added.
data NodeInstruction =
    MakeNode 
        { makeIndex :: Int
        , makeDefault :: Int
        , makeThreshold :: Int
        }
    | MakeEdge 
        { makeFrom :: Int
        , makeTo :: Int
        , makeWeight :: Int
        }
    | MakeOutput
        { outputFrom :: Int
        , outputIndex :: Int
        , outputBit :: Int
        }
    | MakeInput
        { inputTo :: Int
        , inputIndex :: Int
        , inputBit :: Int
        }

-- | Largely for intenal use. A temporary holding structure for nodes that have been parsed from a schema
data NetworkInterim = NetworkInterim
    { interimNodes :: M.Map NID Node
    , interimInputs :: M.Map NID (Int,Int)
    , interimOutputs :: M.Map NID (Int,Int)
    }

-- | Parses a list of `NodeInstruction`s into the temporary holding structure `NetworkInterim`
parseMultiple :: Schema -> NetworkInterim
parseMultiple nis = if and testIndexes then ni else error ("Indexes not in succession! " ++ show ins)
    where
        ni = flip execState (NetworkInterim M.empty M.empty M.empty) . sequence $ parseInstruction <$> nis
        ins = interimNodes ni
        testIndexes = zipWith (\a b -> succ a == b) `ap` tail $ M.keys ins

-- | Internal: Individual instructions are Parsed via pattern matching inside the State monad
parseInstruction :: NodeInstruction -> State NetworkInterim ()
parseInstruction MakeNode{..} = do
    let n = Node makeIndex makeDefault makeDefault makeThreshold [] []
    modify $ \NetworkInterim{..} -> NetworkInterim { interimNodes = M.insert makeIndex n interimNodes,..}
parseInstruction MakeEdge{..} = do
    t <- gets $ \NetworkInterim{..} -> M.lookup makeTo interimNodes
    f <- gets $ \NetworkInterim{..} -> M.lookup makeFrom interimNodes
    let m = t >> (\Node{..} -> Node { edges = (Right makeTo,makeWeight) : edges, ..}) <$> f
    case m of Nothing -> get >>= \NetworkInterim{..} -> error $ "Parse failed, trying to go from " ++ show makeFrom ++ " to " ++ show makeTo ++ " with current map" ++ show interimNodes
              Just n  -> modify $ \NetworkInterim{..} -> NetworkInterim { interimNodes = M.insert makeFrom n interimNodes,..}
parseInstruction MakeOutput{..} = do
    f <- gets $ \NetworkInterim{..} -> M.lookup outputFrom interimNodes
    let mf = (\Node{..} -> Node { edges = (Left (NodeOutput (outputIndex,outputBit)),1) : edges, ..}) <$> f
    case mf of Nothing -> get >>= \NetworkInterim{..} -> error $ "Parse failed, trying to go from " ++ show outputFrom ++ " to output with current map" ++ show interimNodes
               Just n  -> modify $ \NetworkInterim{..} -> NetworkInterim 
                                                            { interimNodes = M.insert outputFrom n interimNodes
                                                            , interimOutputs = M.insert outputFrom (outputIndex,outputBit) interimOutputs
                                                            , ..}
parseInstruction MakeInput {..} = do
    mf <- gets $ \NetworkInterim{..} -> M.lookup inputTo interimNodes
    case mf of Nothing -> get >>= \NetworkInterim{..} -> error $ "Parse failed, trying to go to " ++ show inputTo ++ " from input with current map" ++ show interimNodes
               Just _  -> modify $ \NetworkInterim{..} -> NetworkInterim 
                                                            { interimInputs = M.insert inputTo (inputIndex,inputBit) interimInputs
                                                            , ..}