module AI.MemNet.MemNet where
 
import Control.Concurrent

import AI.MemNet.RunMemNet
import AI.MemNet.MemTest
import AI.MemNet.MemNode
import AI.MemNet.InputOutput
import AI.MemNet.Builder
import AI.MemNet.Server


-- | This will create a MemNet from a schema and begin an execution loop.
--   Some form of UI must be provided to pass through the relevant mutable
--   variables the network needs for input/output. Upon termination of the
--   UI a shutdown command will be sent to the network.
startMemNet :: (NetworkUI ui) => Schema -> NetworkIO ui -> IO ()
startMemNet schema netio = do
    let 
        ui = getUI netio
        shut = getShutdown netio
    mediator <- createMediator schema netio
    writeFile "nodesGraph.gv" $ toGraphViz (mediateNodes mediator) (fromNetwork $ mediateNodeInput mediator) (fromNetwork $ mediateNodeOutput mediator)
    forkOS $ runUI ui
    startServer mediator runMemNet



-- | Execute `runMemNet` with test data
memNetTest :: IO ()
memNetTest = makeTestUI >>= startMemNet testNodes