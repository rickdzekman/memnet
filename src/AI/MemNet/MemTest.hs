{-# LANGUAGE RecordWildCards #-}
module AI.MemNet.MemTest where

import Control.Concurrent
import Control.Monad.State

import Graphics.Gloss.Interface.IO.Game

import AI.MemNet.Builder
import AI.MemNet.InputOutput

-- | The node configuration needed for a 1 bit register
testNodes :: Schema
testNodes =
    -- Nodes
    [ MakeNode 0 10 10
    , MakeNode 1 0  20
    , MakeNode 2 0  20
    , MakeNode 3 0  20
    , MakeNode 4 0  10
    , MakeNode 5 0  10
    --Edges
    , MakeEdge 0 1  10
    , MakeEdge 0 2  10
    , MakeEdge 1 2  10
    , MakeEdge 2 1  10
    , MakeEdge 3 1  20
    , MakeEdge 3 2  20
    , MakeEdge 4 0  (-10)
    , MakeEdge 4 3  10
    , MakeEdge 5 3  10
    -- Outputs
    , MakeOutput 2 0 0 -- From Node 2, to output 0, and bit 0 of that output
    -- Inputs
    , MakeInput 4 0 0 -- Input to Node 4, from input 0, and bit 0 of that input
    , MakeInput 5 0 1 -- Input to Node 5, from input 0, and bit 1 of that input
    ]

-- | Test "World" for the Gloss GUI
data TestWorld = TestWorld
    { downPress :: Bool
    , upPress :: Bool
    , submitPress :: Bool
    , inputValue :: Input [(Int,Bool)]
    , shutdownWorld :: Shutdown
    , outputValue :: Output [(Int,Bool)]
    , outputCycle :: CycleCount
    , worldGate :: CycleGate
    }

-- | Initialise an empty world
emptyTestWorld :: Input [(Int,Bool)] -> Shutdown -> Output [(Int,Bool)] -> CycleCount -> CycleGate -> TestWorld
emptyTestWorld = TestWorld False False False

------------------------------------------------------------
---- GLOSS FUNCTIONS ---------------------------------------
------------------------------------------------------------
drawInterface :: TestWorld -> IO Picture
drawInterface TestWorld{..} = do
    v <- readInputOutput outputValue
    iv <- readInputOutput inputValue
    cc <- readInputOutput outputCycle
    let
        valCurrent = color black . translate (-220) 100 . scale 0.1 0.1 . text $ "Output: " ++ if snd (head v) then "1" else "0"
        cycCurrent = color black . translate (-220) 50 . scale 0.1 0.1 . text $ "Cycle#: " ++ show cc
        waitingBox = color black . translate (-40) 104 $ rectangleWire 40 20
        valWaiting = color black . translate (-44) 100 . scale 0.1 0.1 . text $ if snd (iv !! 1) then "1" else "0"
        upBox = let box = translate 54 104 $ rectangleWire 20 20
                in  if upPress
                        then color yellow box
                        else color black box
        valUp = color black . translate 50 100 . scale 0.1 0.1 . text $ "+"
        downBox = let box = translate 14 104 $ rectangleWire 20 20
                  in  if downPress
                        then color yellow box
                        else color black box
        valDown = color black . translate 10 100 . scale 0.1 0.1 . text $ "-"
        submitBox = let box = translate 154 104 $ rectangleWire 120 30
                    in  if submitPress
                            then color yellow box
                            else color black box
        valSubmit = color black . translate 108 100 . scale 0.1 0.1 $ text  "Enter to send"
        testInstruct = color black . translate 45 50 . scale 0.1 0.1 . text $ "Press UP/DOWN to adjust"
        testInstruct2 = color black . translate 50 (-100) . scale 0.1 0.1 . text $ " Close window to quit"
        inter = pictures [valCurrent,cycCurrent,valWaiting,waitingBox,upBox,valUp,downBox,valDown,submitBox,valSubmit,testInstruct,testInstruct2]
    return inter

handleInput :: Event -> TestWorld -> IO TestWorld
handleInput (EventKey (SpecialKey KeyEnter) Up _ _) tw = return $ tw { submitPress = True }
handleInput (EventKey (SpecialKey KeyUp) Up _ _) tw = return $ tw { upPress = True }
handleInput (EventKey (SpecialKey KeyDown) Up _ _) tw = return $ tw { downPress = True }
handleInput _ tw = return tw

handleStep :: Float -> TestWorld -> IO TestWorld
handleStep _ tw@TestWorld{..} = do
    when upPress $ atomicModifyNthBit inputValue 1 True
    when downPress $ atomicModifyNthBit inputValue 1 False
    if submitPress
        then atomicModifyNthBit inputValue 0 True
        else atomicModifyNthBit inputValue 0 False
    openGate worldGate
    let tws = flip execState tw $ do
            when submitPress
                $ modify $ \tw' -> tw' {submitPress = False}
            when downPress
                $ modify $ \tw' -> tw' {downPress = False}
            when upPress
                $ modify $ \tw' -> tw' {upPress = False}
    return tws
------------------------------------------------------------
---- END GLOSS FUNCTIONS -----------------------------------
------------------------------------------------------------

-- | A newtype so that there can be a NetworkUI instance
newtype TestUI = TestUI { runTestUI :: IO () }

-- | Network UI instance so that the Gloss UI can be run from
--   the `startMemNet` function
instance NetworkUI TestUI where
    runUI = runTestUI

-- | Create the NetworkIO object with the Gloss UI embedded
makeTestUI :: IO (NetworkIO TestUI)
makeTestUI = do
    iwiv <- newInputOutputGroup  [False,False]
    s <- newshutdown
    v <- newInputOutputGroup  [False]
    cc <- newInputOutput 0
    g1 <- newEmptyMVar
    g2 <- newEmptyMVar
    gs <- newInputOutput [g1,g2]
    let tu = testUI iwiv s v cc gs
    return $ NetworkIO tu s cc [iwiv] [v] gs


-- | Load the test Gloss UI
testUI :: Input [(Int,Bool)] -> Shutdown -> Output [(Int,Bool)] -> CycleCount -> CycleGate -> TestUI
testUI iwiv s v cc gs = TestUI $ do
    let tw = emptyTestWorld iwiv s v cc gs
    playIO
        (InWindow "MemNet Test" (500, 300) (400, 100))
        white
        2
        tw
        drawInterface
        handleInput
        handleStep
    shutdown s
