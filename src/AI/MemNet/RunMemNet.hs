{-# LANGUAGE RankNTypes, RecordWildCards, TupleSections #-}
module AI.MemNet.RunMemNet (runMemNet, loopRead, loopSend, loopReset) where

import Data.Array.ST
import Data.Array
import Control.Monad.ST
import Control.Monad
import Data.Foldable (foldl')
import Control.Arrow ((***))
import Data.Either
import Data.Maybe
import qualified Data.Map as M

import AI.MemNet.MemNode
import AI.MemNet.Server
import AI.MemNet.InputOutput

-- A couple of handy custom tools for working with Monads in Tuples
import Control.Monad.Tuple
-- And a few custom tools for grouping lists
import Data.List.Grouping


{- | Have every node with a message read the message 
     and modify its threshold
-}
loopRead :: (forall s. ST s (STArray s Int Node)) -> ST s (STArray s Int Node)
loopRead stpointers = do
    pointers <- stpointers
    nodes <- getElems pointers
    let messaged = filter hasMessage nodes
        readMessage Node{..} = writeArray pointers nodeId
                                Node    { currentState = currentState + sum messages
                                        , messages = []
                                        , .. }
    mapM_ readMessage messaged
    return pointers

{- | Have every node which was triggered during message
    reading send out the next wave of messages
-}
loopSend :: (forall s. ST s (STArray s Int Node)) -> ST s (STArray s Int Node)
loopSend stpointers = do
    pointers <- stpointers
    nodes <- getElems pointers
    let triggered = filter isTriggered nodes
        writeMessage (Node{..},v) = writeArray pointers nodeId Node { messages = v : messages, .. }
        sendMessages Node{..} = do
            gs <- mapM (mjoin' . (readArray pointers *** return)) . rights $ map mfst edges
            mapM_ writeMessage gs
    mapM_ sendMessages triggered
    return pointers

{- | Reset all nodes to their default state
-}
loopReset :: (forall s. ST s (STArray s Int Node)) -> ST s (STArray s Int Node)
loopReset stpointers = do
    pointers <- stpointers
    nodes <- getAssocs pointers
    mapM_ (\(ix,n) -> writeArray pointers ix $ resetNode n) nodes
    return pointers




{- | runMemNet accepts a configrued Mediator as
     its input. Using the various MVars within it will
     loop until receiving the shutdown command.
-}
runMemNet :: Mediator -> IO ()
runMemNet Mediator{..} = memNetLoop mkPointers >> print "Ok. Shutting Down."
    where
        -- Construct a stateful array
        mkPointers :: (forall s. ST s (STArray s Int Node))
        mkPointers = newListArray (0,length mediateNodes - 1) mediateNodes
        -- Quick reference to input array
        inLookups :: Array Int (Array Int NID)
        inLookups = toNetwork mediateNodeInput
        -- Quick reference to output map
        outLookups :: M.Map NID (Int,Int)
        outLookups = fromNetwork mediateNodeOutput
        -- Quick reference to reverse output map
        outLookups' :: Array Int (Array Int NID)
        outLookups' = toNetwork mediateNodeOutput
        -- Recursive loop
        memNetLoop :: (forall s. ST s (STArray s Int Node)) -> IO ()
        memNetLoop stpointers = readInputOutput mediateShutdown >>= \shut -> unless shut $ do
            {-
               Unless shutdown command is given, run through
               this cycle. Start by getting the inpts from
               the input Semaphore in the mediator.
            -}
            ins <- runSemaphore mediateInputSemaphore
                {-
                    For each active node this round, lookup the
                    input reference array, so that an NID can
                    be found for each input.
                -}
            let actThisRnd = foldl'
                                    (\nids (a,xs) ->
                                        let subLookup = inLookups ! a 
                                        in  foldl' (\nids' x -> subLookup ! x : nids') nids xs)
                                    []
                                    ins
                {-
                    stpointers_i is the state of the pointers
                    once input values have been injected.
                -}
                stpointers_i = do
                    arr <- stpointers
                    ns <- readArray arr `mapM` actThisRnd
                    mapM_ (\n -> writeArray arr (nodeId n) $ n { messages = threshold n : messages n }) ns
                    return arr
                {-
                    stpointers_o is the state of the pointers
                    just before resetting all the nodes, thus
                    allowing an output to be extracted.
                -}
                stpointers_o = loopSend $ loopRead stpointers_i
                frozen = runSTArray stpointers_o -- TODO: Use Freeze here?
                {-
                    Once an outut is extracted reset all nodes
                    to allow the loop to resume.
                -}
                stpointers_fin = loopReset stpointers_o
                {-
                    Meanwhile map all of the triggered output
                    nodes to their output functions. For each
                    node that triggers, we need to look up
                    the corresponding bits for their corresponding
                    output. But the output expects a list of booleans,
                    so create a list of booleans of appropriate size,
                    where all of the bits corresponding to active nodes
                    are toggled to True.

                    TODO: This output process is a bit messy. 
                    Should probably get rid of the undisciplined 
                    mapMaybe and use MaybeT / EitherT / STT here.
                -}
                ons =     groupList'
                        . mapMaybe (\Node{..} -> M.lookup nodeId outLookups)
                        . filter isTriggered
                        . map (frozen !)
                        $ M.keys outLookups
            -- The Mediator server will be waiting for the output Semaphore
            putSemaphore mediateOutputSemaphore ons
            -- Once all the phases of running the network are done, resume
            -- the loop, passing forward the latest state.
            memNetLoop stpointers_fin
