{-# LANGUAGE RecordWildCards, TupleSections #-}
module AI.MemNet.MemNode
    ( NID
    , NodeOutput (NodeOutput)
    , NodeGroup
    , Node (Node,nodeId,defaultState,currentState,threshold,messages,edges)
    , hasMessage
    , isTriggered
    , resetNode
    , toGraphViz
    ) where

import Data.List (foldl')
import qualified Data.Map as M

-- | For identifying which Int values correspond to IDs
type NID = Int

-- | Alias for group of Nodes without state
type NodeGroup = [Node]

-- | A mapping for a Node edge to an output
data NodeOutput = NodeOutput (Int,Int) deriving (Show)

{- | Nodes send and receive messages. These messages are 
     simple modifications of their `currentState`. At the end
     of each round a Node will reset its `currentState` to its
     `defaultState` value.

     Sending messages is only triggered when `currentStaet n >= threshold n`
-}
data Node = Node
    { nodeId :: NID -- ^ An ID corresponding to its Ix in the array
    , defaultState :: Int -- ^ When the Node is reset, what should its state go back to
    , currentState :: Int -- ^ Upon construction set to the same as defaultState
    , threshold :: Int -- ^ What value does it need to reach before it will broadcast messages
    , messages :: [Int] -- ^ Incoming messages that modify `currentState`
    , edges :: [(Either NodeOutput NID,Int)] -- ^ `snd . edges` represents the send value of the message, `fst . edges` represents the target Node, or if it is an output
    } deriving (Show)
--

-- | Nodes with messages will process them and modify their current state 
hasMessage :: Node -> Bool
hasMessage Node{..} = not $ null messages

-- | If their current state >= threshold then they are triggered to send messages
isTriggered :: Node -> Bool
isTriggered Node{..} = currentState >= threshold

-- | Reset the Node back to its default state
resetNode :: Node -> Node
resetNode Node{..} = Node { currentState = defaultState, .. }


{- | Creates a string which can be output to GraphViz format
    using `writeFile "nodesGraph.gv" . toGraphViz`. To create
    a PNG version through GraphViz use command line dot tool:
    $ dot -Tpng nodesGraph.gv -o nodesGraph.png
-}
toGraphViz :: [Node] -> M.Map NID (Int,Int) -> M.Map NID (Int,Int) -> String
toGraphViz ns inm oum = graphString
    where
        graphString = foldToGraph ++ inputverts ++ outputverts ++ close
        foldToGraph = foldl' drawVertex (foldl' vertexLabels open ns) ns
        vertexLabels acc Node{..} =   acc
                                   ++ "\r\n V" ++ show nodeId
                                   ++ " [label=\"" ++ "V" ++ show nodeId
                                   ++ " (" ++ show defaultState ++ "/"
                                   ++ show threshold ++ ")\"];"
        drawVertex acc Node{..} = foldl' drawEdge acc edges ++ "\r\n"
            where
                drawEdge accum (Right ix2,v) =    accum
                                               ++ " V" ++ show nodeId ++ " -> "
                                               ++ "V" ++ show ix2
                                               ++ " [label=" ++ show v ++ "];\r\n"
                drawEdge accum (Left (NodeOutput (oi,ox)),_) =
                                                  accum
                                               ++ " V" ++ show nodeId
                                               ++ " -> OUTPUT_" ++ show oi ++ "_" ++ show ox ++ ";\r\n"  
        open = "digraph Maker { \r\n  node [shape=circle,fixedsize=true,width=1.2];\r\n  rankdir=LR;\r\n  size=\"8,6\";\r\n\r\n  "
        outputverts = grpstart ++ outgroup ++ grpend
            where
              (grpstart,grpend) = ("subgraph cluster_output {\r\n    node [shape=square,fixedsize=true,width=1.2];\r\n   label=\"output\";\r\n    color=blue;\r\n","}\r\n")
              outgroup = foldl' (\acc (ox,ob) -> acc ++ " OUTPUT_" ++ show ox ++ "_" ++ show ob ++ ";\r\n") "" $ M.elems oum
        inputverts = grpstart ++ ingroup ++ grpend ++ inlinks
            where
                (grpstart,grpend) = ("subgraph cluster_input {\r\n    node [shape=square,fixedsize=true,width=1.2];\r\n  label=\"inputs\";\r\n    color=blue;\r\n","}\r\n")
                ingroup = foldl' (\acc (ox,ob) -> acc ++ " INPUT_" ++ show ox ++ "_" ++ show ob ++ ";\r\n") "" $ M.elems inm
                inlinks = foldl' (\acc (nix,(iix,iib)) -> acc ++ " INPUT_" ++ show iix ++ "_" ++ show iib ++" -> V" ++ show nix ++ ";\r\n") "" $ M.assocs inm
        close = "\r\n }"